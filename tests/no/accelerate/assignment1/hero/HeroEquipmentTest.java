package no.accelerate.assignment1.hero;

import no.accelerate.assignment1.exceptions.InvalidArmorException;
import no.accelerate.assignment1.exceptions.InvalidWeaponException;
import no.accelerate.assignment1.exceptions.RequiredLevelException;
import no.accelerate.assignment1.item.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class HeroEquipmentTest {
    /*
     *** MAGE EQUIPMENT TESTS ***
     */
    @Test
    public void equip_mage_validWeapon_shouldOccupyWeaponSlot() throws InvalidArmorException, InvalidWeaponException, RequiredLevelException {
        Mage mage = new Mage("Gandalf");
        Weapon validWeapon = new Weapon("Golden Staff", 1, WeaponType.STAFF, 5);

        mage.equip(validWeapon);
        Item actualWeapon = mage.getEquipment().get(Slot.WEAPON);

        assertEquals(validWeapon, actualWeapon);
    }

    @Test
    public void equip_mage_invalidWeapon_shouldThrowException() {
        Mage mage = new Mage("Gandalf");
        Weapon invalidWeapon = new Weapon("B.F. Sword", 1, WeaponType.SWORD, 5);
        String expected = "This weapon (" + invalidWeapon.getName() + ") cannot be used by a " + mage.getClass().getSimpleName();

        Exception exception = assertThrows(InvalidWeaponException.class, () -> mage.equip(invalidWeapon));
        String actual = exception.getMessage();

        assertEquals(expected, actual);
    }

    @Test
    public void equip_mage_validArmor_shouldOccupySlot() throws InvalidArmorException, InvalidWeaponException, RequiredLevelException {
        Mage mage = new Mage("Gandalf");
        HeroAttribute armorAttributes = new HeroAttribute(1, 1,1);
        Armor validArmor = new Armor("Linen Shirt", 1, ArmorType.CLOTH, Slot.BODY, armorAttributes);

        mage.equip(validArmor);
        Item actualArmor = mage.getEquipment().get(Slot.BODY);

        assertEquals(validArmor, actualArmor);
    }

    @Test
    public void equip_mage_invalidArmor_shouldThrowException() {
        Mage mage = new Mage("Gandalf");
        HeroAttribute armorAttributes = new HeroAttribute(2, 1,1);
        Armor invalidArmor = new Armor("Steel Mail", 1, ArmorType.MAIL, Slot.BODY, armorAttributes);
        String expected = "This armor (" + invalidArmor.getName() + ") cannot be used by a " + mage.getClass().getSimpleName() + ".";

        Exception exception = assertThrows(InvalidArmorException.class, () -> mage.equip(invalidArmor));
        String actual = exception.getMessage();

        assertEquals(expected, actual);
    }
    /*
     *** RANGER EQUIPMENT TESTS ***
     */
    @Test
    public void equip_ranger_validWeapon_shouldOccupyWeaponSlot() throws InvalidArmorException, InvalidWeaponException, RequiredLevelException {
        Ranger ranger = new Ranger("Legolas");
        Weapon validWeapon = new Weapon("Hunter's Bow", 1, WeaponType.BOW, 7);

        ranger.equip(validWeapon);
        Item actualWeapon = ranger.getEquipment().get(Slot.WEAPON);

        assertEquals(validWeapon, actualWeapon);
    }

    @Test
    public void equip_ranger_invalidWeapon_shouldThrowException() {
        Ranger ranger = new Ranger("Legolas");
        Weapon invalidWeapon = new Weapon("B.F. Sword", 1, WeaponType.SWORD, 5);
        String expected = "This weapon (" + invalidWeapon.getName() + ") cannot be used by a " + ranger.getClass().getSimpleName();

        Exception exception = assertThrows(InvalidWeaponException.class, () -> ranger.equip(invalidWeapon));
        String actual = exception.getMessage();

        assertEquals(expected, actual);
    }

    @Test
    public void equip_ranger_validArmor_shouldOccupySlot() throws InvalidArmorException, InvalidWeaponException, RequiredLevelException {
        Ranger ranger = new Ranger("Legolas");
        HeroAttribute armorAttributes = new HeroAttribute(1, 1,1);
        Armor validArmor = new Armor("Hunter's Jacket", 1, ArmorType.LEATHER, Slot.BODY, armorAttributes);

        ranger.equip(validArmor);
        Item actualArmor = ranger.getEquipment().get(Slot.BODY);

        assertEquals(validArmor, actualArmor);
    }

    @Test
    public void equip_ranger_invalidArmor_shouldThrowException() {
        Ranger ranger = new Ranger("Legolas");
        HeroAttribute armorAttributes = new HeroAttribute(1, 1, 1);
        Armor invalidArmor = new Armor("White Shirt", 1, ArmorType.CLOTH, Slot.BODY, armorAttributes);
        String expected = "This armor (" + invalidArmor.getName() + ") cannot be used by a " + ranger.getClass().getSimpleName() + ".";

        Exception exception = assertThrows(InvalidArmorException.class, () -> ranger.equip(invalidArmor));
        String actual = exception.getMessage();

        assertEquals(expected, actual);
    }
    /*
     *** ROUGE EQUIPMENT TESTS ***
     */
    @Test
    public void equip_rouge_validWeapon_shouldOccupyWeaponSlot() throws InvalidArmorException, InvalidWeaponException, RequiredLevelException {
        Rouge rouge = new Rouge("Ezzio Auditore Da Firenze");
        Weapon validWeapon = new Weapon("Sting", 1, WeaponType.DAGGER, 4);

        rouge.equip(validWeapon);
        Item actualWeapon = rouge.getEquipment().get(Slot.WEAPON);

        assertEquals(validWeapon, actualWeapon);
    }

    @Test
    public void equip_rouge_invalidWeapon_shouldThrowException() {
        Rouge rouge = new Rouge("Ezzio Auditore Da Firenze");
        Weapon invalidWeapon = new Weapon("Wooden Staff", 1, WeaponType.STAFF, 5);
        String expected = "This weapon (" + invalidWeapon.getName() + ") cannot be used by a " + rouge.getClass().getSimpleName();

        Exception exception = assertThrows(InvalidWeaponException.class, () -> rouge.equip(invalidWeapon));
        String actual = exception.getMessage();

        assertEquals(expected, actual);
    }

    @Test
    public void equip_rouge_validArmor_shouldOccupySlot() throws InvalidArmorException, InvalidWeaponException, RequiredLevelException {
        Rouge rouge = new Rouge("Ezzio Auditore Da Firenze");
        HeroAttribute armorAttributes = new HeroAttribute(1, 1,1);
        Armor validArmor = new Armor("Hunter's Jacket", 1, ArmorType.LEATHER, Slot.BODY, armorAttributes);

        rouge.equip(validArmor);
        Item actualArmor = rouge.getEquipment().get(Slot.BODY);

        assertEquals(validArmor, actualArmor);
    }

    @Test
    public void equip_rouge_invalidArmor_shouldThrowException() {
        Rouge rouge = new Rouge("Ezzio Auditore Da Firenze");
        HeroAttribute armorAttributes = new HeroAttribute(1, 1, 1);
        Armor invalidArmor = new Armor("White Shirt", 1, ArmorType.CLOTH, Slot.BODY, armorAttributes);
        String expected = "This armor (" + invalidArmor.getName() + ") cannot be used by a " + rouge.getClass().getSimpleName() + ".";

        Exception exception = assertThrows(InvalidArmorException.class, () -> rouge.equip(invalidArmor));
        String actual = exception.getMessage();

        assertEquals(expected, actual);
    }
    /*
     *** WARRIOR EQUIPMENT TESTS ***
     */
    @Test
    public void equip_warrior_validWeapon_shouldOccupyWeaponSlot() throws InvalidArmorException, InvalidWeaponException, RequiredLevelException {
        Warrior warrior = new Warrior("Link");
        Weapon validWeapon = new Weapon("The Master Sword", 1, WeaponType.SWORD, 6);

        warrior.equip(validWeapon);
        Item actualWeapon = warrior.getEquipment().get(Slot.WEAPON);

        assertEquals(validWeapon, actualWeapon);
    }

    @Test
    public void equip_warrior_invalidWeapon_shouldThrowException() {
        Warrior warrior = new Warrior("Link");
        Weapon invalidWeapon = new Weapon("Sting", 1, WeaponType.DAGGER, 4);
        String expected = "This weapon (" + invalidWeapon.getName() + ") cannot be used by a " + warrior.getClass().getSimpleName();

        Exception exception = assertThrows(InvalidWeaponException.class, () -> warrior.equip(invalidWeapon));
        String actual = exception.getMessage();

        assertEquals(expected, actual);
    }

    @Test
    public void equip_warrior_validArmor_shouldOccupySlot() throws InvalidArmorException, InvalidWeaponException, RequiredLevelException {
        Warrior warrior = new Warrior("Link");
        HeroAttribute armorAttributes = new HeroAttribute(1, 1,1);
        Armor validArmor = new Armor("Silver Chest", 1, ArmorType.PLATE, Slot.BODY, armorAttributes);

        warrior.equip(validArmor);
        Item actualArmor = warrior.getEquipment().get(Slot.BODY);

        assertEquals(validArmor, actualArmor);
    }

    @Test
    public void equip_warrior_invalidArmor_shouldThrowException() {
        Warrior warrior = new Warrior("Link");
        HeroAttribute armorAttributes = new HeroAttribute(1, 1, 1);
        Armor invalidArmor = new Armor("White Shirt", 1, ArmorType.CLOTH, Slot.BODY, armorAttributes);
        String expected = "This armor (" + invalidArmor.getName() + ") cannot be used by a " + warrior.getClass().getSimpleName() + ".";

        Exception exception = assertThrows(InvalidArmorException.class, () -> warrior.equip(invalidArmor));
        String actual = exception.getMessage();

        assertEquals(expected, actual);
    }
    /*
     *** COMMON EQUIPMENT TESTS ***
     */
    @Test
    public void equip_invalidLevel_shouldThrowException() {
        Warrior warrior = new Warrior("Barbarian");
        Weapon invalidWeapon = new Weapon("Huge Axe", 5, WeaponType.AXE, 10);
        String expected = "Required level for this item is " + invalidWeapon.getRequiredLevel() + ". Your level is " + warrior.getLevel();

        Exception exception = assertThrows(RequiredLevelException.class, () -> warrior.equip(invalidWeapon));
        String actual = exception.getMessage();

        assertEquals(expected, actual);
    }

    @Test
    public void totalAttributes_noArmor_shouldReturnInitialValues() {
        Ranger ranger = new Ranger("Red");
        HeroAttribute expectedAttributes = new HeroAttribute(1, 7, 1); // initial ranger attributes: 1, 7, 1

        HeroAttribute actualAttributes = ranger.totalAttributes();

        assertEquals(expectedAttributes, actualAttributes);
    }
    @Test
    public void totalAttributes_oneArmor_shouldReturnSum() throws InvalidArmorException, InvalidWeaponException, RequiredLevelException {
        Ranger ranger = new Ranger("Red");
        HeroAttribute armorAttribute = new HeroAttribute(2,1,1);
        Armor armor = new Armor("Iron Mail", 1, ArmorType.MAIL, Slot.BODY, armorAttribute);
        HeroAttribute expectedAttributes = new HeroAttribute(1+2, 1+7,1+1);

        ranger.equip(armor);
        HeroAttribute actualAttributes = ranger.totalAttributes();

        assertEquals(expectedAttributes, actualAttributes);
    }
    @Test
    public void totalAttributes_twoArmor_shouldReturnSum() throws InvalidArmorException, InvalidWeaponException, RequiredLevelException {
        Ranger ranger = new Ranger("Red");

        HeroAttribute armorAttribute1 = new HeroAttribute(2,1,1);
        Armor armor1 = new Armor("Iron Mail", 1, ArmorType.MAIL, Slot.BODY, armorAttribute1);

        HeroAttribute armorAttribute2 = new HeroAttribute(2,2,1);
        Armor armor2 = new Armor("Leather shoes", 1, ArmorType.LEATHER, Slot.LEGS, armorAttribute2);

        HeroAttribute expectedAttributes = new HeroAttribute(1+2+2, 7+1+2,1+1+1);

        ranger.equip(armor1);
        ranger.equip(armor2);
        HeroAttribute actualAttributes = ranger.totalAttributes();

        assertEquals(expectedAttributes, actualAttributes);
    }
    @Test
    public void totalAttributes_replaceArmor_shouldReturnNew() throws InvalidArmorException, InvalidWeaponException, RequiredLevelException {
        Ranger ranger = new Ranger("Red");

        HeroAttribute armorAttribute1 = new HeroAttribute(2,1,1);
        Armor armor1 = new Armor("Iron Mail", 1, ArmorType.MAIL, Slot.BODY, armorAttribute1);

        HeroAttribute armorAttribute2 = new HeroAttribute(2,2,1);
        Armor armor2 = new Armor("Leather Jacket", 1, ArmorType.LEATHER, Slot.BODY, armorAttribute2);

        HeroAttribute expectedAttributes = new HeroAttribute(1+2, 7+2,1+1);

        ranger.equip(armor1);
        ranger.equip(armor2);
        HeroAttribute actualAttributes = ranger.totalAttributes();

        assertEquals(expectedAttributes, actualAttributes);
    }
    @Test
    public void damage_noWeapon_shouldReturnDamageAttribute() {
        Warrior warrior = new Warrior("Kratos"); // initial attributes: 5, 2, 1
        double expectedDamage = 1.0 * (1.0+5.0/100.0);

        double actualDamage = warrior.damage();

        assertEquals(expectedDamage, actualDamage);
    }
    @Test
    public void damage_withWeapon_shouldReturnSum() throws InvalidArmorException, InvalidWeaponException, RequiredLevelException {
        Warrior warrior = new Warrior("Kratos");
        Weapon weapon = new Weapon("B.F. Sword", 1, WeaponType.SWORD, 5);
        double expectedDamage = 5.0 * (1.0+5.0/100.0);

        warrior.equip(weapon);
        double actualDamage = warrior.damage();

        assertEquals(expectedDamage, actualDamage);
    }
    @Test
    public void damage_withReplacedWeapon_shouldReturnNewSum() throws InvalidArmorException, InvalidWeaponException, RequiredLevelException {
        Warrior warrior = new Warrior("Kratos");
        Weapon weapon1 = new Weapon("B.F. Sword", 1, WeaponType.SWORD, 5);
        Weapon weapon2 = new Weapon("Mjolnir", 1, WeaponType.HAMMER, 7);
        double expectedDamage = 7.0 * (1.0+5.0/100.0);

        warrior.equip(weapon1);
        // replace the weapon
        warrior.equip(weapon2);
        double actualDamage = warrior.damage();

        assertEquals(expectedDamage, actualDamage);
    }
    @Test
    public void damage_withArmorAndWeapon_shouldReturnSum() throws InvalidArmorException, InvalidWeaponException, RequiredLevelException {
        Warrior warrior = new Warrior("Kratos");
        Weapon weapon = new Weapon("B.F. Sword", 1, WeaponType.SWORD, 5);
        HeroAttribute armorAttribute = new HeroAttribute(3,2,1);
        Armor armor = new Armor("Silver Mail", 1, ArmorType.MAIL, Slot.BODY, armorAttribute);
        double expectedDamage = 5.0 * (1.0+(5.0+3.0)/100.0);

        warrior.equip(weapon);
        warrior.equip(armor);
        double actualDamage = warrior.damage();

        assertEquals(expectedDamage, actualDamage);
    }
}
