package no.accelerate.assignment1.hero;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {
    /*
    *** MAGE TESTS ***
    */
    @Test
    public void create_mage_shouldHaveCorrectName() {
        // Arrange
        String expectedName = "Gandalf";
        // Act
        Hero mage = new Mage(expectedName);
        String actualName = mage.getName();
        // Assert
        assertEquals(expectedName, actualName);
    }

    @Test
    public void create_mage_shouldHaveCorrectLevel() {
        int expectedLevel = 1;

        Hero mage = new Mage("Gandalf");
        int actualLevel = mage.getLevel();

        assertEquals(expectedLevel, actualLevel);
    }

    @Test
    public void create_mage_shouldHaveCorrectAttributes() {
        HeroAttribute expectedAttributes = new HeroAttribute(1,1,8);
        Hero mage = new Mage("Gandalf");

        HeroAttribute actualAttributes = mage.getLevelAttributes();

        assertEquals(expectedAttributes, actualAttributes);
    }

    @Test
    public void levelUp_mage_shouldHaveCorrectLevel() {
        int expectedLevel = 2;
        Hero mage = new Mage("Gandalf");

        mage.levelUp();
        int actualLevel = mage.getLevel();

        assertEquals(expectedLevel, actualLevel);
    }

    @Test
    public void levelUp_mage_shouldHaveCorrectAttributes() {
        Hero mage = new Mage("Gandalf");
        HeroAttribute expectedAttributes = new HeroAttribute(1+1,1+1,8+5);

        mage.levelUp();
        HeroAttribute actualAttributes = mage.getLevelAttributes();

        assertEquals(expectedAttributes, actualAttributes);
    }

    @Test
    public void display_mage_shouldHaveCorrectState() {
        Hero mage = new Mage("Gandalf");
        String expectedState = """
                *** YOUR HERO ***

                Name: Gandalf
                Class: Mage
                Level: 1
                Total strength: 1
                Total dexterity: 1
                Total intelligence: 8
                Total damage: 1.08""";

        String actualState = mage.display();

        assertEquals(expectedState,actualState);
    }
    /*
     *** RANGER TESTS ***
     */
    @Test
    public void create_ranger_shouldHaveCorrectName() {
        // Arrange
        String expectedName = "Lara Croft";
        // Act
        Hero ranger = new Ranger(expectedName);
        String actualName = ranger.getName();
        // Assert
        assertEquals(expectedName, actualName);
    }

    @Test
    public void create_ranger_shouldHaveCorrectLevel() {
        int expectedLevel = 1;

        Hero ranger = new Ranger("Lara Croft");
        int actualLevel = ranger.getLevel();

        assertEquals(expectedLevel, actualLevel);
    }

    @Test
    public void create_ranger_shouldHaveCorrectAttributes() {
        HeroAttribute expectedAttributes = new HeroAttribute(1,7,1);
        Hero ranger = new Ranger("Lara Croft");

        HeroAttribute actualAttributes = ranger.getLevelAttributes();

        assertEquals(expectedAttributes, actualAttributes);
    }

    @Test
    public void levelUp_ranger_shouldHaveCorrectLevel() {
        int expectedLevel = 2;
        Hero ranger = new Ranger("Lara Croft");

        ranger.levelUp();
        int actualLevel = ranger.getLevel();

        assertEquals(expectedLevel, actualLevel);
    }

    @Test
    public void levelUp_ranger_shouldHaveCorrectAttributes() {
        Hero ranger = new Ranger("Lara Croft");
        HeroAttribute expectedAttributes = new HeroAttribute(1+1,7+5,1+1);

        ranger.levelUp();
        HeroAttribute actualAttributes = ranger.getLevelAttributes();

        assertEquals(expectedAttributes, actualAttributes);
    }
    @Test
    public void display_ranger_shouldHaveCorrectState() {
        Hero ranger = new Ranger("Lara Croft");
        String expectedState = """
                *** YOUR HERO ***

                Name: Lara Croft
                Class: Ranger
                Level: 1
                Total strength: 1
                Total dexterity: 7
                Total intelligence: 1
                Total damage: 1.07""";

        String actualState = ranger.display();

        assertEquals(expectedState,actualState);
    }

    /*
     *** ROUGE TESTS ***
     */
    @Test
    public void create_rouge_shouldHaveCorrectName() {
        // Arrange
        String expectedName = "Vy";
        // Act
        Hero rouge = new Rouge(expectedName);
        String actualName = rouge.getName();
        // Assert
        assertEquals(expectedName, actualName);
    }

    @Test
    public void create_rouge_shouldHaveCorrectLevel() {
        int expectedLevel = 1;

        Hero rouge = new Rouge("Vy");
        int actualLevel = rouge.getLevel();

        assertEquals(expectedLevel, actualLevel);
    }

    @Test
    public void create_rouge_shouldHaveCorrectAttributes() {
        HeroAttribute expectedAttributes = new HeroAttribute(2,6,1);
        Hero rouge = new Rouge("Vy");

        HeroAttribute actualAttributes = rouge.getLevelAttributes();

        assertEquals(expectedAttributes, actualAttributes);
    }

    @Test
    public void levelUp_rouge_shouldHaveCorrectLevel() {
        int expectedLevel = 2;
        Hero rouge = new Rouge("Vy");

        rouge.levelUp();
        int actualLevel = rouge.getLevel();

        assertEquals(expectedLevel, actualLevel);
    }

    @Test
    public void levelUp_rouge_shouldHaveCorrectAttributes() {
        Hero rouge = new Rouge("Vy");
        HeroAttribute expectedAttributes = new HeroAttribute(2+1,6+4,1+1);

        rouge.levelUp();
        HeroAttribute actualAttributes = rouge.getLevelAttributes();

        assertEquals(expectedAttributes, actualAttributes);
    }
    @Test
    public void display_rouge_shouldHaveCorrectState() {
        Hero rouge = new Rouge("Vy");
        String expectedState = """
                *** YOUR HERO ***

                Name: Vy
                Class: Rouge
                Level: 1
                Total strength: 2
                Total dexterity: 6
                Total intelligence: 1
                Total damage: 1.06""";

        String actualState = rouge.display();

        assertEquals(expectedState,actualState);
    }

    /*
     *** WARRIOR TESTS ***
     */
    @Test
    public void create_warrior_shouldHaveCorrectName() {
        // Arrange
        String expectedName = "Leonidas";
        // Act
        Hero warrior = new Warrior(expectedName);
        String actualName = warrior.getName();
        // Assert
        assertEquals(expectedName, actualName);
    }

    @Test
    public void create_warrior_shouldHaveCorrectLevel() {
        int expectedLevel = 1;

        Hero warrior = new Warrior("Leonidas");
        int actualLevel = warrior.getLevel();

        assertEquals(expectedLevel, actualLevel);
    }

    @Test
    public void create_warrior_shouldHaveCorrectAttributes() {
        HeroAttribute expectedAttributes = new HeroAttribute(5,2,1);
        Hero warrior = new Warrior("Leonidas");

        HeroAttribute actualAttributes = warrior.getLevelAttributes();

        assertEquals(expectedAttributes, actualAttributes);
    }

    @Test
    public void levelUp_warrior_shouldHaveCorrectLevel() {
        int expectedLevel = 2;
        Hero warrior = new Warrior("Leonidas");

        warrior.levelUp();
        int actualLevel = warrior.getLevel();

        assertEquals(expectedLevel, actualLevel);
    }

    @Test
    public void levelUp_warrior_shouldHaveCorrectAttributes() {
        Hero warrior = new Warrior("Leonidas");
        HeroAttribute expectedAttributes = new HeroAttribute(5+3,2+2,1+1);

        warrior.levelUp();
        HeroAttribute actualAttributes = warrior.getLevelAttributes();

        assertEquals(expectedAttributes, actualAttributes);
    }
    @Test
    public void display_warrior_shouldHaveCorrectState() {
        Hero warrior = new Warrior("Leonidas");
        String expectedState = """
                *** YOUR HERO ***

                Name: Leonidas
                Class: Warrior
                Level: 1
                Total strength: 5
                Total dexterity: 2
                Total intelligence: 1
                Total damage: 1.05""";

        String actualState = warrior.display();

        assertEquals(expectedState,actualState);
    }

}
