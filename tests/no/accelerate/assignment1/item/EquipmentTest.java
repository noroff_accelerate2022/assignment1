package no.accelerate.assignment1.item;

import no.accelerate.assignment1.hero.HeroAttribute;
import no.accelerate.assignment1.item.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class EquipmentTest {
    /*
     *** WEAPON TESTS ***
     */
    @Test
    public void create_weapon_shouldHaveCorrectName() {
        String expectedName = "The Master Sword";
        Weapon weapon = new Weapon("The Master Sword", 1, WeaponType.SWORD, 5);

        String actualName = weapon.getName();

        assertEquals(expectedName, actualName);
    }

    @Test
    public void create_weapon_shouldHaveRequiredLevel() {
        int expectedRequiredLevel = 1;
        Weapon weapon = new Weapon("The Master Sword", 1, WeaponType.SWORD, 5);

        int actualRequiredLevel = weapon.getRequiredLevel();

        assertEquals(expectedRequiredLevel, actualRequiredLevel);
    }

    @Test
    public void create_weapon_shouldHaveCorrectType() {
        WeaponType expectedType = WeaponType.SWORD;
        Weapon weapon = new Weapon("The Master Sword", 1, WeaponType.SWORD, 5);

        WeaponType actualType = weapon.getType();

        assertEquals(expectedType, actualType);
    }

    @Test
    public void create_weapon_shouldHaveCorrectSlot() {
        Slot expectedSlot = Slot.WEAPON;
        Weapon weapon = new Weapon("The Master Sword", 1, WeaponType.SWORD, 5);

        Slot actualSlot = weapon.getSlot();

        assertEquals(expectedSlot, actualSlot);
    }

    @Test
    public void create_weapon_shouldHaveCorrectDamage() {
        int expectedDamage = 4;
        Weapon weapon = new Weapon("The Master Sword", 1, WeaponType.SWORD, 4);

        int actualDamage = weapon.getDamage();

        assertEquals(expectedDamage, actualDamage);
    }

    /*
     *** ARMOR TESTS ***
     */
    @Test
    public void create_armor_shouldHaveCorrectName() {
        String expectedName = "Iron Plate";
        HeroAttribute armorAttributes = new HeroAttribute(2,3,1);
        Armor armor = new Armor("Iron Plate", 1, ArmorType.PLATE, Slot.BODY, armorAttributes);

        String actualName = armor.getName();

        assertEquals(expectedName, actualName);
    }

    @Test
    public void create_armor_shouldHaveRequiredLevel() {
        int expectedRequiredLevel = 1;
        HeroAttribute armorAttributes = new HeroAttribute(2,3,1);
        Armor armor = new Armor("Iron Plate", 1, ArmorType.PLATE, Slot.BODY, armorAttributes);

        int actualRequiredLevel = armor.getRequiredLevel();

        assertEquals(expectedRequiredLevel, actualRequiredLevel);
    }

    @Test
    public void create_armor_shouldHaveCorrectType() {
        ArmorType expectedType = ArmorType.PLATE;
        HeroAttribute armorAttributes = new HeroAttribute(2,3,1);
        Armor armor = new Armor("Iron Plate", 1, ArmorType.PLATE, Slot.BODY, armorAttributes);

        ArmorType actualType = armor.getType();

        assertEquals(expectedType, actualType);
    }

    @Test
    public void create_armor_shouldHaveCorrectSlot() {
        Slot expectedSlot = Slot.BODY;
        HeroAttribute armorAttributes = new HeroAttribute(2,3,1);
        Armor armor = new Armor("Iron Plate", 1, ArmorType.PLATE, Slot.BODY, armorAttributes);

        Slot actualSlot = armor.getSlot();

        assertEquals(expectedSlot, actualSlot);
    }

    @Test
    public void create_armor_shouldHaveCorrectAttributes() {
        HeroAttribute expectedAttributes = new HeroAttribute(2,3,1);
        Armor armor = new Armor("Iron Plate", 1, ArmorType.PLATE, Slot.BODY, expectedAttributes);

        HeroAttribute actualAttributes = armor.getArmorAttribute();

        assertEquals(expectedAttributes, actualAttributes);
    }
}
