package no.accelerate.assignment1.item;

public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
