package no.accelerate.assignment1.item;

public class Weapon extends Item {
    private WeaponType type;
    private int damage;

    public Weapon(String name, int requiredLevel, WeaponType type, int damage) {
        super(name, requiredLevel);
        this.type = type;
        this.damage = damage;
        this.slot = Slot.WEAPON;
    }

    public WeaponType getType() {
        return type;
    }

    public int getDamage() {
        return damage;
    }

}
