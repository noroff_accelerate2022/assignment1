package no.accelerate.assignment1.item;

public enum Slot {
    WEAPON,
    HEAD,
    BODY,
    LEGS
}
