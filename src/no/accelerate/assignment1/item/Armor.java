package no.accelerate.assignment1.item;

import no.accelerate.assignment1.hero.HeroAttribute;

public class Armor extends Item {
    private ArmorType type;
    HeroAttribute armorAttribute;

    public Armor(String name, int requiredLevel, ArmorType type, Slot slot, HeroAttribute armorAttribute) {
        super(name, requiredLevel);
        this.type = type;
        this.slot = slot;
        this.armorAttribute = armorAttribute;
    }

    public ArmorType getType() {
        return type;
    }

    public HeroAttribute getArmorAttribute() {
        return armorAttribute;
    }
}
