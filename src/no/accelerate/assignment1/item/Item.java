package no.accelerate.assignment1.item;

public abstract class Item {
    private String name;
    private int requiredLevel;
    protected Slot slot;

    public Item(String name, int requiredLevel) {
        this.name = name;
        this.requiredLevel = requiredLevel;
    }

    public String getName() {
        return name;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public Slot getSlot() {
        return slot;
    }
}
