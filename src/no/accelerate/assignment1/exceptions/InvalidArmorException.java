package no.accelerate.assignment1.exceptions;

import no.accelerate.assignment1.item.Armor;

public class InvalidArmorException extends Exception {
    public InvalidArmorException(String heroClass, String armorName) {

        super("This armor (" + armorName + ") cannot be used by a " + heroClass + ".");
    }
}
