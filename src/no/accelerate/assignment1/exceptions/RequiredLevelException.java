package no.accelerate.assignment1.exceptions;

public class RequiredLevelException extends Exception {
    public RequiredLevelException(int requiredLevel, int heroLevel) {
        super("Required level for this item is " + requiredLevel + ". Your level is " + heroLevel);
    }
}
