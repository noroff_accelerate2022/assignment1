package no.accelerate.assignment1.exceptions;

import no.accelerate.assignment1.item.Weapon;

public class InvalidWeaponException extends Exception {
    public InvalidWeaponException(String heroClass, String weaponName) {

        super("This weapon (" + weaponName + ") cannot be used by a " + heroClass);
    }
}
