package no.accelerate.assignment1.hero;

import java.util.ArrayList;
import java.util.HashMap;

import no.accelerate.assignment1.exceptions.InvalidArmorException;
import no.accelerate.assignment1.exceptions.InvalidWeaponException;
import no.accelerate.assignment1.exceptions.RequiredLevelException;
import no.accelerate.assignment1.item.*;
import no.accelerate.assignment1.item.Slot;

public abstract class Hero {
    private final String name;
    protected int level;
    protected HashMap<Slot, Item> equipment;
    protected HeroAttribute levelAttributes;
    protected ArrayList<WeaponType> validWeaponType;
    protected ArrayList<ArmorType> validArmorType;

    public Hero(String name) {
        this.name = name;
        this.level = 1;
        this.equipment = initEquipment();
    }

    // internal method to generate an empty equipment when initialising the object
    private HashMap<Slot, Item> initEquipment() {
        HashMap<Slot, Item> eq = new HashMap<>();

        for (Slot slotType : Slot.values()) {
            eq.put(slotType, null);
        }
        return eq;
    }

    // internal method to calculate appropriate attributes depending on the hero class
    private HeroAttribute calculateLevelAttributes() {
        if (this instanceof Mage) {
            return new HeroAttribute(1,1,5);
        } else if (this instanceof Ranger) {
            return new HeroAttribute(1,5,1);
        } else if (this instanceof Rouge) {
            return new HeroAttribute(1,4,1);
        } else {
            return new HeroAttribute(3,2,1);
        }
    }

    // internal method to calculate appropriate damage attribute depending on the hero class
    private int calculateDamageAttribute()  {
        HeroAttribute totalAttributes = this.totalAttributes();
        if (this instanceof Mage) {
            return totalAttributes.getIntelligence();
        } else if (this instanceof Ranger || this instanceof Rouge) {
            return totalAttributes.getDexterity();
        } else {
            return totalAttributes.getStrength();
        }
    }

    public void equip(Item item) throws InvalidWeaponException, InvalidArmorException, RequiredLevelException {
        // don't go any further if required level is too high
        if (item.getRequiredLevel() <= this.level) {
            if ((item.getSlot() == Slot.WEAPON)) {
                if (this.validWeaponType.contains(((Weapon) item).getType())) {
                    this.equipment.put(item.getSlot(), item);
                } else {
                    throw new InvalidWeaponException(this.getClass().getSimpleName(), item.getName());
                }
            } else {
                if (this.validArmorType.contains(((Armor) item).getType())) {
                    this.equipment.put(item.getSlot(), item);
                } else {
                    throw new InvalidArmorException(this.getClass().getSimpleName(), item.getName());
                }
            }
        } else {
            throw new RequiredLevelException(item.getRequiredLevel(), this.level);
        }
    }

    public HeroAttribute totalAttributes() {
        int totalStrength = this.levelAttributes.getStrength();
        int totalDexterity = this.levelAttributes.getDexterity();
        int totalIntelligence = this.levelAttributes.getIntelligence();

        for (Slot slot : this.equipment.keySet()) {
            Item currentItem = this.equipment.get(slot);

            // only armor items contribute to the sum
            if (currentItem != null && currentItem.getSlot() != Slot.WEAPON) {
                HeroAttribute currBonus = ((Armor) currentItem).getArmorAttribute();

                totalStrength += currBonus.getStrength();
                totalDexterity += currBonus.getDexterity();
                totalIntelligence += currBonus.getIntelligence();
            }
        }

        return new HeroAttribute(totalStrength, totalDexterity, totalIntelligence);
    }
    public double damage() {
        int damageAttribute = this.calculateDamageAttribute();
        Item weapon = this.equipment.get(Slot.WEAPON);

        int weaponDamage = weapon == null ? 1 : ((Weapon) weapon).getDamage();

        return weaponDamage * (1.0 + damageAttribute/100.0);
    }

    public String display() {
        HeroAttribute totals = this.totalAttributes();
        StringBuilder heroInfo = new StringBuilder();

        heroInfo.append("*** YOUR HERO ***\n\n");
        heroInfo.append("Name: ").append(this.name).append("\n");
        heroInfo.append("Class: ").append(this.getClass().getSimpleName()).append("\n");
        heroInfo.append("Level: ").append(this.level).append("\n");
        heroInfo.append("Total strength: ").append(totals.getStrength()).append("\n");
        heroInfo.append("Total dexterity: ").append(totals.getDexterity()).append("\n");
        heroInfo.append("Total intelligence: ").append(totals.getIntelligence()).append("\n");
        heroInfo.append("Total damage: ").append(this.damage());

        return heroInfo.toString();
    }

    public void levelUp() {
        HeroAttribute levelAttributes = this.calculateLevelAttributes();

        this.levelAttributes.addAttributes(levelAttributes);
        this.level++;
    };

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public HeroAttribute getLevelAttributes() {
        return levelAttributes;
    }
    public HashMap<Slot, Item> getEquipment() {
        return equipment;
    }

}
