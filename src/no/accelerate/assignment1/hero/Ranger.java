package no.accelerate.assignment1.hero;

import no.accelerate.assignment1.item.ArmorType;
import no.accelerate.assignment1.item.WeaponType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Ranger extends Hero {
    public Ranger(String name) {
        super(name);
        this.levelAttributes = new HeroAttribute(1, 7, 1);
        this.validWeaponType = new ArrayList<WeaponType>(Arrays.asList(WeaponType.BOW));
        this.validArmorType = new ArrayList<ArmorType>(Arrays.asList(ArmorType.LEATHER, ArmorType.MAIL));
    }
}
