package no.accelerate.assignment1.hero;

import no.accelerate.assignment1.item.ArmorType;
import no.accelerate.assignment1.item.Item;
import no.accelerate.assignment1.item.Weapon;
import no.accelerate.assignment1.item.WeaponType;
import no.accelerate.assignment1.item.Slot;

import java.util.ArrayList;
import java.util.Arrays;

public class Warrior extends Hero {
    public Warrior(String name) {
        super(name);
        this.levelAttributes = new HeroAttribute(5,2,1);
        this.validWeaponType = new ArrayList<WeaponType>(Arrays.asList(WeaponType.AXE, WeaponType.HAMMER, WeaponType.SWORD));
        this.validArmorType = new ArrayList<ArmorType>(Arrays.asList(ArmorType.MAIL, ArmorType.PLATE));
    }
}
