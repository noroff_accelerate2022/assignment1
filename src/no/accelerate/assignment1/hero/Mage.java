package no.accelerate.assignment1.hero;

import no.accelerate.assignment1.item.ArmorType;
import no.accelerate.assignment1.item.WeaponType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Mage extends Hero {
    public Mage(String name) {
        super(name);
        this.levelAttributes = new HeroAttribute(1, 1, 8);
        this.validWeaponType = new ArrayList<>(Arrays.asList(WeaponType.STAFF, WeaponType.WAND));
        this.validArmorType = new ArrayList<>(Arrays.asList(ArmorType.CLOTH));
    }
}
