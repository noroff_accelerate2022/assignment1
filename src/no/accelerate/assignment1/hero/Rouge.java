package no.accelerate.assignment1.hero;

import no.accelerate.assignment1.item.ArmorType;
import no.accelerate.assignment1.item.WeaponType;

import java.util.ArrayList;
import java.util.Arrays;

public class Rouge extends Hero {
    public Rouge(String name) {
        super(name);
        this.levelAttributes = new HeroAttribute(2, 6, 1);
        this.validWeaponType = new ArrayList<WeaponType>(Arrays.asList(WeaponType.DAGGER, WeaponType.SWORD));
        this.validArmorType = new ArrayList<ArmorType>(Arrays.asList(ArmorType.LEATHER, ArmorType.MAIL));
    }
}
