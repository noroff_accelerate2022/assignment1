package no.accelerate.assignment1.hero;

public class HeroAttribute {
    private int strength;
    private int dexterity;
    private int intelligence;

    public HeroAttribute(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    // only alters THIS object, does not return anything
    public void addAttributes(HeroAttribute levelUp) {
        this.strength += levelUp.strength;
        this.dexterity += levelUp.dexterity;
        this.intelligence += levelUp.intelligence;
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    // for testing purposes
    @Override
    public boolean equals(Object obj) {
        return obj instanceof HeroAttribute
                && this.strength == ((HeroAttribute) obj).strength
                && this.dexterity == ((HeroAttribute) obj).dexterity
                && this.intelligence == ((HeroAttribute) obj).intelligence;
    }
}
