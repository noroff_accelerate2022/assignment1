# Assignment1 - RPG Heroes

The project is an initial structure for an RPG game, representing various Heroes, as well as items than can be equipped in the game.

One can build upon those structures in order to create a simple, text-based game engine and allow the Heroes to interact with each other, fight the monsters, improve the equipment, gain the experience, etc.

## Heroes
Following Heroes can be created in the game:
- Mage
- Ranger
- Rouge
- Warrior

The attributes are being updated automatically upon creation and levelling up. The only custom property of a hero is the name.

## Items
There is a wide range of equipment. One can create the following weapons:
- Axe
- Bow
- Dagger
- Hammer
- Staff
- Sword
- Wand

Each weapon has a custom name, required level, and damage.

These armor types can be created in the game:
- Cloth,
- Leather,
- Mail,
- Plate

Each armor piece has custom name, slot it occupies (Body, Legs, Head) and attributes, which contribute to Hero's skills.

## Setup
The program has test coverage of 93% for classes and 99% for lines of code.

The repository has a CI pipeline that runs the test suite every time a new change is pushed to master.

There is currently no functionality in the Main class.

